# Logging & Reports stack

This docker-compose.yml avail these services:

- **Grafana** dashboard manager
- **Elasticsearch** as a background database to save all noSQL data
- **Graylog** platform to bring the posibility to integrate docker logs through graylog driver
- **MongoDB** to save data for auth and another graylog's stuff

## How to run

- Just run `$ docker-compose up -d`
- At first time it will create *mongodb* and *elasticsearch* instances from scratch.
- Then all data generated will be persistant in a volume.

## How to start sending logs to that graylog service.
**TODO**

## How to connect elasticsearch DB to start using dashboards.

**TODO**

## Default users

**TODO**

